# Mikrotik interfaces addresses configuration.

# Warning! The role deletes ALL undefined IPv4 addresses. You must be carefull to not loose control over your device!

A Role allows configuraiton of Mikrotik RouterOS Interfaces addresses IPv4 only.

Role allows setup of IPv4 static addresses and DHCP client.


Examples of usage with comments are in docs/

Big thanks to Martin Dulin for his role https://github.com/mikrotik-ansible/mikrotik-firewall.
His role gave me an idea how solve RouterOS configuration tasks.
