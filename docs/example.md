mikrotik_interfaces_addresses_ipv4:
  delete_all_addresses: false # If true - deletes ALL static IPv4 addresses
  delete_all_dhcp_clients: false # If true - deletes ALL DHCP clients

  delete_addresses_on_target_router: true # If true - deletes import-script on the target router

  static_addresses:
    - comment: (string) # Required. Description of the address
      disabled: (yes|no) # Required. If the address is disabled
      address: (ipv4 address with prefix length) # Required. IPv4 address. Must be in form 10.8.0.1/24
      interface: (string) # Required. Name of interface to bind the address to.

  dhcp_clients:
    - interface: (string) # Required. name of an interface to bind DHCP client to
      comment: (string) # Required. Description of a client
      disabled: (yes|no) # Required. Whether to disable DHCP client.
      add-default-route: (yes | no | special-classless) # Default: yes. Whether to install default route in routing table received from dhcp server. By default RouterOS client complies to RFC and ignores option 3 if classless option 121 is received. To force client not to ignore option 3 set special-classless. This parameter is available in v6rc12+. yes - adds classless route if received, if not then add default route (old behavior). special-classless - adds both classless route if received and default route (MS style)
      dhcp-options: (string) # Default: None. DHCP options to send to DHCP server. Default list is: clientid, clientid_duid, hostname. List, separated by comma.
      script: (string) # Default: None. Script to run on receiveing IPv4 address from DHCP server.
      use-peer-ntp: (yes|no) # Default: yes. Whether to accept the NTP server settings from DHCP server.
      default-route-distance: (number) # Default: 1. Sets default route distance for a route received from DHCP server.
      use-peer-dns: (yes|no) # Default: yes. Whether to accept the DNS server settings from DHCP server.
